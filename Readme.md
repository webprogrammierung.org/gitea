## Linux Installieren

```

chmod +x gitea


```


## Backup Server

```
scp benuzer name@server Name:/root/gitea/gitea/*.zip /test

```

## Algemeine Einstellung


* app.ini datei Beispiel


```
ENABLE_LETSENCRYPT=true
LETSENCRYPT_ACCEPTTOS=true
LETSENCRYPT_DIRECTORY=https
LETSENCRYPT_EMAIL=thorstenkloehn@gmail.com
PROTOCOL=https
SSH_DOMAIN       = webprogrammierung.org
DOMAIN           = webprogrammierung.org
HTTP_PORT        = 443
ROOT_URL         = https://webprogrammierung.org/
LANDING_PAGE = explore

```

## Git Spieglung

* [Webprogrammierung.org](https://webprogrammierung.org/webprogrammierung.org/gitea.git)
* [Codenerg](https://codeberg.org/webprogrammierung.org/gitea.git)
* 
